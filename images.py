import asyncpraw, const, random

from const import send_embed


REDDIT_ID = '3XWnv0qUhIDPMaMUL2iUew'
REDDIT_TOKEN = 'sKTBL7UTZxGKUIreF0KN-F41QuKCOw'
REDDIT_AGENT = 'Image Getter'
REDDIT_USERNAME = 'vojtechzak2'
REDDIT_PASSWORD = 'asd456qwe789'

EXTENSIONS = ['.jpg', '.jpeg', '.png', '.gif']


class Imager:

	def __init__(self):
		
		self.reddit = asyncpraw.Reddit(

			client_id = REDDIT_ID,
			client_secret = REDDIT_TOKEN,
			user_agent = REDDIT_AGENT,
			username = REDDIT_USERNAME,
			password = REDDIT_PASSWORD
		)


	async def send_reddit_image(self, author, channel, query):

		subreddit = await self.find_subreddit(query)

		if not subreddit:
			await send_embed(channel, f'No subreddit found for `{query}`', author)
			return
			
		'''
		if subreddit.over18 and not channel.is_nsfw():
			await channel.send(f'I cannot post NSFW content in this channel')
			return
		'''

		url = await self.get_post_url(subreddit)
		await send_embed(channel, f'Image from `r/{subreddit}`', author)
		await channel.send(url)


	async def find_subreddit(self, query):

		try:
			subreddit = await self.reddit.subreddits.search_by_name(query).__anext__()
		except StopAsyncIteration:
			return False

		await subreddit.load()
		return subreddit


	async def get_post_url(self, subreddit):

		print(f'subreddit: {subreddit}')

		post = await subreddit.random()

		if post is None:
			post = await self.get_broken_post(subreddit)
		
		else:
			while not any(post.url.endswith(ext) for ext in EXTENSIONS):
				post = await subreddit.random()

		return post.url
	

	async def get_broken_post(self, subreddit):

		hots = subreddit.hot()

		for i in range(random.randint(0, 50)):
			post = await hots.__anext__()

		while not any(post.url.endswith(ext) for ext in EXTENSIONS):
			post = await hots.__anext__()

		return post



class ImagerHandler:

	def __init__(self):

		self.imager = Imager()
		self.commands = {}

		events = [
			[self.c_image, const.C_IMAGE]
		]

		for event in events:
			for command in event[1]:
				self.commands[command] = event[0]
	
	
	async def handle(self, message):
		await self.commands[message.content[0]](message)


	async def c_image(self, message):

		query = message.content[1]
		channel = message.channel
		author = message.author
		await self.imager.send_reddit_image(author, channel, query)
