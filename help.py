import const


class Helper:
	
	async def send_help(self, message):

		prefixes = f"`{'`, `'.join(const.PREFIXES)}`"
		text1 = f'I recognize these command prefixes: {prefixes}'
		text2 = 'Type `-commands` to learn what I can do'

		await message.channel.send(f'{text1}\n{text2}')

	async def send_comms(self, message):
		pass


class HelperHandler:
	
	def __init__(self):

		self.helper = Helper()
		self.commands = {}

		events = [
			[self.c_help, const.C_HELP],
			[self.c_comm, const.C_COMM]
		]

		for event in events:
			for command in event[1]:
				self.commands[command] = event[0]


	async def handle(self, message):
		await self.commands[message.content[0]](message)


	async def c_help(self, message):
		await self.helper.send_help(message)

	async def c_comm(self, message):
		await self.helper.send_comms(message)



