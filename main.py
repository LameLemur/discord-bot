#!/usr/bin/env python3

import discord
import const, help, music, images


class MessageHandler:

	def __init__(self, client):

		self.helper_h = help.HelperHandler()
		self.player_h = music.PlayerHandler(client)
		self.imager_h = images.ImagerHandler()
		self.commands = {}

		events = [
			[self.helper_h, self.helper_h.commands],
			[self.player_h, self.player_h.commands],
			[self.imager_h, self.imager_h.commands]
		]

		for event in events:
			for command in event[1]:
				self.commands[command] = event[0]


	async def handle(self, message: discord.Message):

		if message.content[0] not in self.commands:
			await message.channel.send(f'Invalid command: `{message.content[0]}`')
			##print("test je tohle vole")
			return
		await self.commands[message.content[0]].handle(message)


if __name__ == '__main__':

	client = discord.Client()
	message_h = MessageHandler(client)

	# command testing
	@client.event
	async def on_ready():

		# await client.get_channel(889581781756567603).send('-p stacys mom')
		# await client.get_channel(889581781756567603).send('-p scotty doesnt know')
		# await client.get_channel(889581781756567603).send('-p kanikuli')
		pass


	@client.event
	async def on_message(message: discord.Message):

		if message.author.bot and message.author.name != 'DiscordBot':
			return

		if not message.content:
			return

		if message.content[0] not in const.PREFIXES:
			return

		comm = message.content.split(' ')[0][1:]
		args = message.content.split(' ', 1)[-1]
		message.content = [comm, args]

		await message_h.handle(message)


	client.run(const.DISCORD_TOKEN)
