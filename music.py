import asyncio, discord, math, youtube_dl
import const
import random
import queue
import collections
from const import send_embed

from pprint import pprint

import spotipy
from spotipy.oauth2 import SpotifyClientCredentials

YDL_OPTIONS = {'format':"bestaudio"}
FFMPEG_OPTIONS = {'before_options': '-reconnect 1 -reconnect_streamed 1 -reconnect_delay_max 5', 'options': '-vn'}



class Player:

	def __init__(self, vc):
		self.spotify = spotipy.Spotify(client_credentials_manager=SpotifyClientCredentials(const.SPOTIFY_TOKEN, const.SPOTIFY_PASSWORD))
		self.vc : discord.VoiceClient = vc
		self.queue = collections.deque()
		self.played = collections.deque(maxlen=50)
		self.currently_playing : Song
		self.stopped = False


	async def pause(self, author, channel):

		if self.vc.is_paused():
			await send_embed(channel, "I'm already paused", author)
			return

		self.vc.pause()
		await send_embed(channel, "Paused", author)


	async def resume(self, author, channel):

		if not self.vc.is_paused():
			await send_embed(channel, "I'm already playing", author)
			return

		self.vc.resume()
		await send_embed(channel, 'Resumed', author)


	async def stop_player(self, author, channel):

		if not self.vc.is_playing():
			await send_embed(channel, "I'm already stopped", author)
			return
		self.stop()
		await send_embed(channel, 'Stopped', author)

	def stop(self):
		self.stopped = True
		self.vc.stop()

	async def play(self):

		try:
			url = self.currently_playing.url
		except:
			self.currently_playing = self.queue.popleft()
			url = self.currently_playing.url
		ydl = youtube_dl.YoutubeDL(YDL_OPTIONS)
		try:
			info = ydl.extract_info(url, download=False)
		except:
			print(url)
			print('error cannot download url from queue')
			return

		self.update_song_file()
		stream_url = info['formats'][0]['url']
		source = await discord.FFmpegOpusAudio.from_probe(stream_url, **FFMPEG_OPTIONS)
		self.vc.play(source, after=self.after_song)

	def after_song(self, error):

		if self.stopped:
			self.stopped = False
			return
		try:
			self.played.append(self.currently_playing)
			self.currently_playing = self.queue.popleft()
			asyncio.run(self.play())
		except:
			self.currently_playing = None
			self.update_song_file()
			self.stop()
			print("queue empty")

	async def find_on_youtube(self, query):

		ydl = youtube_dl.YoutubeDL(YDL_OPTIONS)
		try:
			info = ydl.extract_info(query, download=False)
		except:
			info = ydl.extract_info(f'ytsearch:{query}', download=False)['entries'][0]

		if '_type' in info and info['_type'] == 'playlist':
			songs = [Song(entry['title'].split("(")[0].split("[")[0], f"https://youtube.com/watch?v={entry['id']}", entry['duration']) for entry in info['entries']]
		else:
			songs = [Song(info['title'].split("(")[0].split("[")[0], f"https://youtube.com/watch?v={info['id']}", info['duration'])]
		return songs

	def get_spotify_list(self, query):

		tracks = []
		id = query.split('/')[4].split('?')[0]
		if "album" in query:
			results = self.spotify.album_tracks(id)['items']
			for result in results:
				tracks.append(result['artists'][0]['name'] + " " + result['name'])
		elif "track" in query:
			results = self.spotify.track(id)
			tracks.append(results['album']['artists'][0]['name'] + " " + results['name'])
		elif "playlist" in query:
			results = self.spotify.playlist_tracks(id)["items"]
			for result in results:
				tracks.append(result['track']['album']['artists'][0]['name'] + " " + result['track']['name'])
		elif "artist" in query:
			results = self.spotify.artist_top_tracks(id)['tracks']
			for result in results:
				tracks.append(result['album']['artists'][0]['name'] + " " + result['name'])
		if "-shuffle" in query:
			random.shuffle(tracks)
		return tracks

	async def enqueue(self, query, author, channel, append_method):
		
		first = True
		if "spotify" in query:
			song_list = self.get_spotify_list(query)
			for song_string in song_list:
				songs = await self.find_on_youtube(song_string)
				await asyncio.sleep(1)
				for song in songs:
					append_method(song)
					if first:
						first = False
						if not self.vc.is_playing():
							await self.play()
		else:
			songs = await self.find_on_youtube(query)
			for song in songs:
				append_method(song)
				if first:
					first = False
					if not self.vc.is_playing():
						await self.play()
		await send_embed(channel, 'The queue adding complete', author)

	async def queue_print(self, author, channel):

		if not len(self.queue):
			try:
				text = f'Playing: {self.currently_playing.title}{self.currently_playing.get_duration()}\n'
			except:
				text = 'The queue is empty'
		else:
			text = f'Playing: {self.currently_playing.title}{self.currently_playing.get_duration()}\nQueue:\n'
			for i, song in enumerate(self.queue):
				text +=  f'{i+1}. {song.title}{song.get_duration()}\n\n'
				if i > 13:
					text += f'16. - {len(self.queue)}.\n'
					break
		text = f'```{text}```'
		await channel.send(text)

	async def queue_skip(self, channel, author):
		try:
			new_song = self.queue.popleft()
			self.played.append(self.currently_playing)
			self.currently_playing = new_song
		except:
			await send_embed(channel, "Couldn't skip, out of range", author)
			return
		self.stop()
		await self.play()

	async def queue_prev(self, channel, author):
		try:
			new_song = self.played.pop()
			self.queue.appendleft(self.currently_playing)
			self.currently_playing = new_song
			self.stop()
			await self.play()
		except:
			await send_embed(channel, "Couldn't go to previous, empty played list.", author)

	def update_song_file(self):

		try:
			site = const.WEBSITE.replace('toBeReplaced', self.currently_playing.title)
		except:
			site = const.WEBSITE.replace('toBeReplaced', 'nothing is currenlty_playing')
		with open("/web/current_song.html", "w", encoding="utf-8") as text_file: ##replace with a path for your web server folder
			text_file.write(site)

class PlayerHandler:

	def __init__(self, client):

		self.client = client
		self.players = {}
		self.commands = {}

		events = [
			[self.c_join, const.C_JOIN],
			[self.c_quit, const.C_QUIT],

			[self.c_pause, const.C_PAUSE],
			[self.c_resume, const.C_RESUME],
			[self.c_stop, const.C_STOP],

			[self.c_play_next, const.C_PLAY_NEXT],
			[self.c_play_last, const.C_PLAY_LAST],

			[self.c_queue, const.C_QUEUE],
			[self.c_next, const.C_NEXT],
			[self.c_prev, const.C_PREV]
		]

		for event in events:
			for command in event[1]:
				self.commands[command] = event[0]


	async def handle(self, message):

		channel = message.channel
		author = message.author

		if not author.voice:
			await send_embed(channel, "You have to be in a voice channel", author)
			return

		try:
			player = self.players[str(message.guild.id)]
		except:
			player = None

		enabled = const.C_JOIN + const.C_PLAY_NEXT + const.C_PLAY_LAST

		if not message.content[0] in enabled and not player:
			await send_embed(channel, "I'm not connected to any channel", author)
			return

		await self.commands[message.content[0]](player, message)


	async def c_join(self, player, message):

		channel = message.channel
		author = message.author

		if player and player.vc.is_connected():
			await send_embed(channel, 'I already am in a voice channel', author)
			return

		await author.voice.channel.connect()

		vc = self.client.voice_clients[-1]
		player = Player(vc)

		self.players[str(message.guild.id)] = player

		await send_embed(channel, f'Joined channel {author.voice.channel.name}', author)



	async def c_quit(self, player, message):

		await player.vc.disconnect()
		self.players.pop(str(message.guild.id))


	async def c_pause(self, player, message):
		await player.pause(message.author, message.channel)

	async def c_resume(self, player, message):
		await player.resume(message.author, message.channel)

	async def c_stop(self, player, message):
		await player.stop_player(message.author, message.channel)


	async def c_play_next(self, player, message):

		if not player:
			await self.c_join(player, message)
			await self.handle(message)
			return

		author = message.author
		channel = message.channel
		query = message.content[1]
		if query == "-p":
			if player.vc.is_paused():
				await player.resume(message.author, message.channel)
			else:
				await player.pause(message.author, message.channel)
		else:
			await player.enqueue(query, author, channel, player.queue.appendleft)


	async def c_play_last(self, player, message):

		if not player:
			await self.c_join(player, message)
			await self.handle(message)
			return

		author = message.author
		channel = message.channel
		query = message.content[1]
		if query == "-p":
			if player.vc.is_paused():
				await player.resume(message.author, message.channel)
			else:
				await player.pause(message.author, message.channel)
		else:
			await player.enqueue(query, author, channel, player.queue.append)


	async def c_queue(self, player, message):
		await player.queue_print(message.author, message.channel)

	async def c_next(self, player, message):
		await player.queue_skip(message.channel, message.author)

	async def c_prev(self, player, message):
		await player.queue_prev(message.channel, message.author)




class Song:

	def __init__(self, title, url, duration):

		self.title = title
		self.url = url
		self.duration = duration

	def get_duration(self):

		sec = str(self.duration % 60).zfill(2)
		min = str(int(self.duration / 60)).rjust(2)

		return f'{min}:{sec}'
