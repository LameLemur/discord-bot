import discord

NAME = 'DiscordBot'
PREFIXES = ['!', '-', '?', '/', ';']
DISCORD_TOKEN = 'ODg3OTk2MTEwODU2MTMwNjIw.YUMQ0g.UIRKQMctOpb-LB7P_p0LZjRafCU'
SPOTIFY_TOKEN = '1542704b185142e1b08aed1356adee1b'
SPOTIFY_PASSWORD = 'c347f9add81e495585e662e71cee3f67'
WEBSITE = "<html>\
<head>\
<title>Current Preceptor Song</title>\
<meta http-equiv=\"refresh\" content=\"8\">\
<meta charset=\"UTF-8\">\
</head>\
<body>\
<marquee behavior=\"scroll\" direction=\"left\">toBeReplaced</marquee>\
</body>\
</html>"


async def send_embed(channel, text, author):

	mention = f'[<@{author.id}>]'
	embed = discord.Embed(description = f'{text} {mention}')
	await channel.send(embed = embed)


# helper commands
C_HELP = ['h', 'help']
C_COMM = ['c', 'commands']


# player commands
C_JOIN = ['j', 'join']
C_QUIT = ['quit', 'gtfo', 'pdpc', 'paldopice', 'seeya', 'leave', 'cya', 'papa']

C_PAUSE = ['pause']
C_RESUME = ['resume']
C_STOP = ['stop']

C_PLAY_NEXT = ['pn', 'playnext']
C_PLAY_LAST = ['p', 'pl', 'play', 'playlast']

C_QUEUE = ['q', 'queue']
C_NEXT = ['n', 'next', 's', 'skip']
C_PREV = ['b', 'back', 'prev', 'previous', 'pr']


# imager commands
C_IMAGE = ['img', 'pic', 'pls']
